# Meta
A simple project which contains files/resources that may be used across the Desktop Framework Libraries Group. It's also used as a generic
issues tracker.

## Desktop Framework Libraries
DFL is a collection of pure Qt libraries that are useful in building various components of a DE. These libraries are intended to be platform
independent (X11/Wayland).

## Resources
 - Uncrustify.cfg - All contributors are requested to run uncrustify using [this](https://gitlab.com/desktop-frameworks/meta/-/blob/main/uncrustify.cfg) configuration file so that we may have a uniform code style.
 - Compile script - A simple pythonic [compile](https://gitlab.com/desktop-frameworks/meta/-/blob/main/compile.py) script is available to compile and install the entire DF Project.

## Issues Tracker
Issues that may be opened here:
 - Issues that may not belong to any specific project (ex: a request for a new DF class)
 - Issues that may belong to multiple projects (ex: problems regarding uncrustify).
 - If the user does not know in which project to open an issue, they may do so here.
 - Problems with the resources need to be reported here.

Please note: Issues opened here may be moved to suitable project without prior notice. In such cases, the said issue will be closed here and a new issue will be created in the taget project.
