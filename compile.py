#!/usr/bin/python3
# -*- encoding: utf-8 -*-

"""
compile - Sequentially compile the whole DFL project

###================== Program Info ==================###
    Program Name : compile
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys
    Python Version : Python 3.4 or higher
    Author : Marcus Britanicus
    Email : marcusbritanicus@gmail.com
    License : Public Domain
###==================================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # This script is in public domain; do whatever you want to do with it.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys

Projects = [
    'IPC',
    'Login1',
    'Utils',
    'XDG',
    'Applications',
    'ColorUtils',
    'ConfigParser',
    'Inotify',
    'Keyring',
    'Layouts',
    'Meta',
    'Notifier',
    'Settings',
    'SNI',
    'Storage',
    'Volume',
    'WayQt',
    'WlrClipboard',
    'WlrGammaEffects',
    'Power',
    # 'Examples',
]


ProjectsInfo = {
	"Meta" :            ( "Meta",            "https://gitlab.com/desktop-frameworks/meta.git" ),
	"IPC" :             ( "IPC",             "https://gitlab.com/desktop-frameworks/ipc.git" ),
	"Utils" :           ( "Utils",           "https://gitlab.com/desktop-frameworks/utils.git" ),
	"Applications" :    ( "Applications",    "https://gitlab.com/desktop-frameworks/applications.git" ),
	"ColorUtils" :      ( "ColorUtils",      "https://gitlab.com/desktop-frameworks/color-utils.git" ),
	"ConfigParser" :    ( "ConfigParser",    "https://gitlab.com/desktop-frameworks/config-parser.git" ),
	"Inotify" :         ( "Inotify",         "https://gitlab.com/desktop-frameworks/inotify.git" ),
	"Keyring" :         ( "Keyring",         "https://gitlab.com/desktop-frameworks/keyring.git" ),
	"Layouts" :         ( "Layouts",         "https://gitlab.com/desktop-frameworks/layouts.git" ),
	"Login1" :          ( "Login1",          "https://gitlab.com/desktop-frameworks/login1.git" ),
	"Notifier" :        ( "Notifier",        "https://gitlab.com/desktop-frameworks/notification.git" ),
	"Power" :           ( "Power",           "https://gitlab.com/desktop-frameworks/power.git" ),
	"SNI" :             ( "SNI",             "https://gitlab.com/desktop-frameworks/status-notifier.git" ),
	"Settings" :        ( "Settings",        "https://gitlab.com/desktop-frameworks/settings.git" ),
	"Storage" :         ( "Storage",         "https://gitlab.com/desktop-frameworks/storage.git" ),
	"Volume" :          ( "Volume",          "https://gitlab.com/desktop-frameworks/volume.git" ),
	"WayQt" :           ( "WayQt",           "https://gitlab.com/desktop-frameworks/wayqt.git" ),
	"WlrClipboard" :    ( "WlrClipboard",    "https://gitlab.com/desktop-frameworks/clipboard.git" ),
	"WlrGammaEffects" : ( "WlrGammaEffects", "https://gitlab.com/desktop-frameworks/gamma-effects.git" ),
	"XDG" :             ( "XDG",             "https://gitlab.com/desktop-frameworks/xdg.git" ),
	"Examples" :        ( "Examples",        "https://gitlab.com/desktop-frameworks/examples.git" ),
}

def uninstall( BuildDir ) :

    for proj in Project:
        if not os.path.exists( f"{ProjectsInfo[proj][0]}/{BuildDir}/meson-logs/install-log.txt" ):
            print( f"{proj} not installed. Nothing to uninstall." )
            continue

        os.system( f"cd {ProjectsInfo[proj][0]}; sudo ninja -C .build uninstall" )


if __name__ == '__main__' :

    import argparse

    parser = argparse.ArgumentParser( description = 'Compile and install the Desktop Framework Libraries', add_help = False )
    parser.add_argument( "-h", '--help', help = 'Print this help and exit', action = "help" )
    parser.add_argument(
        "-i", '--install',
        help = "Install the project.",
        dest = "install",
        action = "store_true"
    )

    parser.add_argument(
        "-u", '--uninstall',
        help = "Uninstall the project.",
        dest = "uninstall",
        action = "store_true"
    )

    parser.add_argument(
        "-p", '--prefix',
        help = "Install everything to <prefix>. Defaults to '/usr'. Provide absolute path.",
        dest = "prefix",
        default = "/usr"
    )

    parser.add_argument(
        "-c", '--clean',
        help = "Remove the existing build directory before compilation.",
        dest = "clean",
        action = "store_true",
    )

    parser.add_argument(
        "-q", '--qt-version',
        help = "Choose the Qt version to use for compilation.",
        dest = "qtver",
        default = "qt5"
    )

    args = parser.parse_args()


    QtVer = "-Duse_qt_version=qt5"
    Build = ".build"
    if args.qtver in ["qt6", "6"]:
        QtVer = "-Duse_qt_version=qt6"
        Build = ".build6"

    if ( not args.install and not args.uninstall ):
        print( "Please specify at least one of --install or --uninstall\n" )

        parser.print_help()

    elif args.install :
        for proj in Projects:
            ## We don't need to compile Meta.
            if proj == "Meta":
                continue

            print( proj )

            # This project is not cloned yet: Clone it.
            if not os.path.exists( ProjectsInfo[ proj ][ 0 ] ):
                os.system( f"git clone {ProjectsInfo[ proj ][ 1 ]} {ProjectsInfo[ proj ][ 0 ]}" )
                os.chdir( ProjectsInfo[ proj ][ 0 ] )

            # This project exists: Update it.
            else:
                os.chdir( ProjectsInfo[ proj ][ 0 ] )
                os.system( f"git checkout main; git pull --recurse-submodules --all" )

            # If the build directory exists, and we are tasked to clean it.
            if os.path.exists( Build ) and args.clean:
                shutil.rmtree( Build )

            ret = 0
            if not os.path.exists( Build ):
                while True:
                    ret = os.system( f"meson setup {Build} --prefix={args.prefix} --buildtype=release {QtVer}" )
                    if not ret:
                        break;

                    input( "Errors encountered while running meson. Review the code, create or make edits to meson.build and hit Enter to proceed." )

            # Proceed only if compilation was successful.
            while True:
                ret = os.system( f"ninja -C {Build} -k 0 -j $(nproc)" )
                if not ret:
                    break;

                input( "Errors encountered while building. Review the code, make edits and hit Enter to proceed." )

            # Proceed only if installation was successful.
            while True:
                ret = os.system( f"sudo ninja -C {Build} install" )
                if ( not ret ):
                    break;

                input( "Errors encountered while installing. Review the install instructions and hit Enter to proceed." )

            os.chdir( '../' )
            if ( '/' in ProjectsInfo[ proj ][ 0 ] ):
                os.chdir( '../' )

            print()


    elif args.uninstall :
        uninstall( Build )
